#!/bin/bash

if [[ $# -eq 0 ]]; then
    echo "Give target directory"

    exit 0
fi

find "$1" -depth -name '*' | while read FILE; do
    DIR=$(dirname "$FILE")
    OLD=$(basename "$FILE")
    NEW=$(echo "$OLD" | tr ' ' '_')

    if [[ "$OLD" != "$NEW" ]]; then
        mv -i "$DIR/$OLD" "$DIR/$NEW"
        echo "$DIR/$OLD ---> $DIR/$NEW"
    fi
done

exit 0
