#!/bin/bash

# Fixes the `steam` package for AMD drivers due to missing libs.

lsb_installed=`pacman -Qs lsb-release`

if [[ -n "$lsb_installed" ]]; then
    echo "Installing lsb-release, missing lib."

    sudo pacman -S lsb-release
fi

font_installed=`pacman -Qs ttf-liberation`

if [[ -n "$font_installed" ]]; then
    echo "Installing ttf-liberation, a font used by Steam."

    sudo pacman -S ttf-liberation
fi

echo "Deleting some compiled files."
find ~/.steam/root/ \( -name "libgcc_s.so*" -o -name "libstdc++.so*" -o -name "libxcb.so*" \) -print -delete

DRIVERS_INSTALLED=0

while :; do
    read -p "Do you have your GPUs drivers installed? [Y/n]" yn

    case $yn in
        [Yy]* ) DRIVERS_INSTALLED=1; break;;
        [Nn]* ) break;;
        * ) echo "Yes or no.";;
    esac
done

if [[ "$DRIVERS_INSTALLED" -eq 1 ]]; then
    echo "Nothing else to do here."

    exit 0
fi

echo "You need to install your GPUs drivers. As of December 25th, 2015, these"\
     "are:"
echo "AMD/ATI:"
echo "- Open source: xf86-video-amdgpu + lib32-mesa-libgl"
echo "    OR xf86-video-ati + lib32-mesa-libgl"
echo "- Proprietary (AUR): catalyst + lib32-catalyst-libgl"
echo "Intel:"
echo "- Open source: xf86-video-intel + lib32-mesa-libgl"
echo "Nvidia:"
echo "- Open source: xf86-video-nouveau + lib32-mesa-libgl"
echo "- Proprietary: nvidia + lib32-nvidia-libgl"
echo "  OR nvidia-340xx + lib32-nvidia-340xx-libgl"
echo "  OR nvidia-304xx + lib32-nvidia-304xx-libgl"

exit 0
