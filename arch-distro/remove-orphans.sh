#!/bin/bash

# Retrieves a list of orphaned packages and their then-unneeded dependencies
# where possible.
sudo pacman -Rs $(pacman -Qtdq)
