# My personal Arch installation.
# Not commentated.
#
# It is recommended you start from here:
#
# https://wiki.archlinux.org/index.php/Beginners'_guide
#
# Most importantly partitioning will most likely be different from this script.

wifi-menu

timedatectl set-ntp true

# Partitioning
lsblk

## parted
parted /dev/sdx

mklabel msdos
mkpart primary ext4 1MiB 100%
set 1 boot on

## parted-end


mkfs.ext4 /dev/sdxy

mount /dev/sdxy /mnt

# Partitioning-end

pacstrap -i /mnt base base-devel

genfstab -U /mnt > /mnt/etc/fstab

arch-chroot /mnt /bin/bash

# arch-chroot [bash]

locale-gen

echo LANG=en_US.UTF-8 > /etc/locale.conf

tzselect

ln -s /usr/share/zoneinfo/America/Los_Angeles /etc/localtime

hwclock --systohc --utc

mkinitcpio -p linux

#: BIOS/MBR
pacman -S grub os-prober

grub-install --recheck /dev/sdx

grub-mkconfig -o /boot/grub/grub.cfg

echo akarin > /etc/hostname

pacman -S iw wpa_supplicant dialog

echo "[archlinuxfr]
SigLevel = Never
Server = http://repo.archlinux.fr/$arch" >> /etc/pacman.conf

pacman -Sy

pacman -S yaourt

#: Change password.
passwd

#: Add non-root user, replace USER_NAME with name of user.
useradd -m -g users -G wheel,storage,power -s /bin/bash USER_NAME
passwd USER_NAME

#: Install sudo.
pacman -S sudo

#: Install zsh and neovim.
pacman -S zsh

#: Remove vi, vim, and nano.
pacman -R vi vim nano

# Install xorg.
pacman -S xorg-server xorg-xinit xorg-server-utils
